# Yu-Gi-Oh! Card Info Server

Copyright (C) 2020, 2021 Sam and Janis Allen
samjanis@mirrorisland.com

Copyright (C) 2021 Aladdin Allen
aladdintrainz2016@gmail.com

The Yu-Go-Oh! Card Template Server (referred to as "ygocards") is an SQL database with a PHP backend script to fetch the card data.

Originally, this project was designed as a template generator for Mirror Island (www.mirrorisland.com) so people could quickly list cards without having to type the card info themselves. It also provides a *suggested* price based on a basic algorithm.

However this project now has enough features to become a standalone web service. A demo frontend written in HTML can be used to access the ygocards database as a quick way of getting card data or card price in Australian dollars.

At the time of writing, the latest working version can be accessed on:
https://ygocards.mirrorisland.com/

Also, the latest MySQL dump is available for download.


## What this currently DOES do

- Generate card listing and fetch prices.
- Search by card name in English (such as "Dark Magician")
- Search by set code (e.g. "DUSA 100")


## What this currently DOESN'T do

- Show prices in currencies other than Australian dollars.
- Contain data for non-English cards.
- Search by card text or other data (such as atk/def or type/attribute)


Better documentation as well as build instructions will be added soon.

## Credits

EDOPro and ProjectIgnis: Card texts and data.

yugioprices.com: Card price API.

exchangeratesapi.io: Currency conversion rates.