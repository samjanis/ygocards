<?php
header('Access-Control-Allow-Origin: *');

require('connectdbinfo.php');

$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname)
		or die("Could not connect: ".mysqli_connect_error());
$mysqli->set_charset('utf8mb4');

/**
 * Convenience function to handle database calls.
 */
function ygocards_php_query($query)
{ 
	global $mysqli;
	$result = mysqli_query($mysqli, $query) or die ("MariaDB/MySQL error ".mysqli_errno($mysqli)
			.": ".mysqli_error($mysqli)."\n<br>\n$query");

	return $result; 
}

/**
 * Convenience function to escape strings for SQL commands.
 */
function ygocards_php_escape($string)
{
	global $mysqli;

	return mysqli_real_escape_string($mysqli, $string);
}

/**
 * Clean text input. Also trims source $string to $stringlength (required).
 */
function ygocards_php_cleantext($string, $stringlength)
{ 
	return substr(trim(stripslashes($string)), 0, $stringlength);
}

/**
 * Obtain integer values from a string.
 */
function ygocards_php_inttext($string)
{
	$inttext = preg_replace('/[^0-9\-]/', '', $string);
	$inttext = substr($inttext, 0, 12);
	$inttext = intval($inttext);
	return $inttext;
}

/* Const values. Credit goes to Project Ignis/EDOPro
 * (and possible others) for these values. */
define('CARDTYPE_MONSTER', 0x1);
define('CARDTYPE_SPELL', 0x2);
define('CARDTYPE_TRAP', 0x4);
define('CARDTYPE_NORMAL', 0x10);
define('CARDTYPE_EFFECT', 0x20);
define('CARDTYPE_FUSION', 0x40);
define('CARDTYPE_RITUAL', 0x80);
define('CARDTYPE_TRAPMONSTER', 0x100);
define('CARDTYPE_SPIRIT', 0x200);
define('CARDTYPE_UNION', 0x400);
define('CARDTYPE_GEMINI', 0x800);
define('CARDTYPE_TUNER', 0x1000);
define('CARDTYPE_SYNCHRO', 0x2000);
define('CARDTYPE_TOKEN', 0x4000);
define('CARDTYPE_QUICKPLAY', 0x10000);
define('CARDTYPE_CONTINUOUS', 0x20000);
define('CARDTYPE_EQUIP', 0x40000);
define('CARDTYPE_FIELD', 0x80000);
define('CARDTYPE_COUNTER', 0x100000);
define('CARDTYPE_FLIP', 0x200000);
define('CARDTYPE_TOON', 0x400000);
define('CARDTYPE_XYZ', 0x800000);
define('CARDTYPE_PENDULUM', 0x1000000);
define('CARDTYPE_SPSUMMON', 0x2000000);
define('CARDTYPE_LINK', 0x4000000);

define('ATTRIBUTE_EARTH', 0x01);
define('ATTRIBUTE_WATER', 0x02);
define('ATTRIBUTE_FIRE', 0x04);
define('ATTRIBUTE_WIND', 0x08);
define('ATTRIBUTE_LIGHT', 0x10);
define('ATTRIBUTE_DARK', 0x20);
define('ATTRIBUTE_DIVINE', 0x40);

/* Monster Types */
define('MONSTERTYPE_WARRIOR', 0x1);
define('MONSTERTYPE_SPELLCASTER', 0x2);
define('MONSTERTYPE_FAIRY', 0x4);
define('MONSTERTYPE_FIEND', 0x8);
define('MONSTERTYPE_ZOMBIE', 0x10);
define('MONSTERTYPE_MACHINE', 0x20);
define('MONSTERTYPE_AQUA', 0x40);
define('MONSTERTYPE_PYRO', 0x80);
define('MONSTERTYPE_ROCK', 0x100);
define('MONSTERTYPE_WINGEDBEAST', 0x200);
define('MONSTERTYPE_PLANT', 0x400);
define('MONSTERTYPE_INSECT', 0x800);
define('MONSTERTYPE_THUNDER', 0x1000);
define('MONSTERTYPE_DRAGON', 0x2000);
define('MONSTERTYPE_BEAST', 0x4000);
define('MONSTERTYPE_BEASTWARRIOR', 0x8000);
define('MONSTERTYPE_DINOSAUR', 0x10000);
define('MONSTERTYPE_FISH', 0x20000);
define('MONSTERTYPE_SEASERPENT', 0x40000);
define('MONSTERTYPE_REPTILE', 0x80000);
define('MONSTERTYPE_PSYCHIC', 0x100000);
define('MONSTERTYPE_DIVINE', 0x200000);
define('MONSTERTYPE_CREATORGOD', 0x400000);
define('MONSTERTYPE_WYRM', 0x800000);
define('MONSTERTYPE_CYBERSE', 0x1000000);

/* Link arrows (stored in Def.) */
define('LINK_MARKER_BOTTOM_LEFT', 0x1);
define('LINK_MARKER_BOTTOM', 0x2);
define('LINK_MARKER_BOTTOM_RIGHT', 0x4);
define('LINK_MARKER_LEFT', 0x8);
define('LINK_MARKER_TOP_RIGHT', 0x100);
define('LINK_MARKER_RIGHT', 0x20);
define('LINK_MARKER_TOP_LEFT', 0x40);
define('LINK_MARKER_TOP', 0x80);

/** Begin execution from here. **/

/* Start to fetch card data. */
$cardid = ygocards_php_cleantext($_GET['id'], 64);

if (!$cardid)
	return false;

$query = sprintf('select * from ygocards where cardid = "%s" limit 1;',
		ygocards_php_escape($cardid));

$result = ygocards_php_query($query);
if (!mysqli_num_rows($result))
	return false;

$carddata = mysqli_fetch_assoc($result);
$setcode = "{$carddata['setprefix']}-{$carddata['setnumber']}";

/* Get current exchange rate for USD/AUD */
$audrate = false;
$getexchangerate = true;
$today = date("Y-m-d");
if (file_exists('data/ygocards-exchangerate.php')) {
	/* Fetch $audrate and $lastupdate from an external PHP file. */
	require('data/ygocards-exchangerate.php');

	/* If a day has passed, clear the rate to get a new one. */
	if (strtotime($lastupdate) == strtotime($today)) {
		$getexchangerate = false;
	}
}

if ($getexchangerate) {
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,
			"https://api.exchangeratesapi.io/latest?base=USD&symbols=AUD");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	$response = curl_exec($ch);
	curl_close($ch);

	if ($response) {
		$res = json_decode($response);
		$newrate = $res->rates->AUD;

		if ($newrate) {
			$audrate = $newrate;
			$txt = sprintf('<?php $audrate=%f; $lastupdate="%s"; ?>',
					$audrate, $today);
			file_put_contents('data/ygocards-exchangerate.php', $txt);
			chmod('data/ygocards-exchangerate.php', 0666);
		}
	}
}

$pricehigh = $carddata['pricehigh'];
$pricelow = $carddata['pricelow'];
$priceaverage = $carddata['priceaverage'];
$priceupdated = $carddata['priceupdated'];
if (strtotime($priceupdated) < strtotime($today)) {
	// * Fetch card prices. * /
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,
			"http://yugiohprices.com/api/price_for_print_tag/{$setcode}");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	$response = curl_exec($ch);
	curl_close($ch);

	$status = false;
	$res = json_decode($response);
	if ($res) {
		$status = $res->status;
	}
	if ($status) {
		$newpricehigh = $res->data->price_data->price_data->data->prices->high;
		$newpricelow = $res->data->price_data->price_data->data->prices->low;
		$newpriceaverage = $res->data->price_data->price_data->data->prices->average;

		$pricehigh = $newpricehigh * $audrate;
		$pricelow = $newpricelow * $audrate;
		$priceaverage = $newpriceaverage * $audrate;

		$query = sprintf('update ygocards set pricehigh=%f, pricelow=%f,'
				.' priceaverage=%f, priceupdated="%s"'
				.' where ygocards.id=%d limit 1',
				$pricehigh, $pricelow, $priceaverage,
				$today, $carddata['id']);
		ygocards_php_query($query);
	}
}

if ($priceaverage < 2.5) {
	/* Prices less than $2.50 round up to nearest 10c */
	$price = ceil($priceaverage * 10) / 10;
} else if ($priceaverage < 10) {
	/* Prices between $2.50 and $10 round up to nearest 50c */
	$price = ceil($priceaverage * 2) / 2;
} else {
	/* Prices above $10 round up to nearest dollar */
	$price = ceil($priceaverage);
}
$priceformatted = sprintf('AU$%0.2f', $price);
$pricehighformatted = sprintf('AU$%0.2f', $pricehigh);
$pricelowformatted = sprintf('AU$%0.2f', $pricelow);
$priceaverageformatted = sprintf('AU$%0.2f', $priceaverage);

/* Set card type specifics. */
$monsterleveltypeattribute = '';

/* Get monster attributes. */
$attribute = $carddata['attribute'];
$attributename = '';
if ($attribute & ATTRIBUTE_EARTH) {
	$attributename = 'Earth';
} else if ($attribute & ATTRIBUTE_WATER) {
	$attributename = 'Water';
} else if ($attribute & ATTRIBUTE_FIRE) {
	$attributename = 'Fire';
} else if ($attribute & ATTRIBUTE_WIND) {
	$attributename = 'Wind';
} else if ($attribute & ATTRIBUTE_LIGHT) {
	$attributename = 'Light';
} else if ($attribute & ATTRIBUTE_DARK) {
	$attributename = 'Dark';
} else if ($attribute & ATTRIBUTE_DIVINE) {
	$attributename = 'Divine';
}

/* Get monster types. */
$monstertype = $carddata['race'];
$monstertypenames = '';
if ($monstertype & MONSTERTYPE_WARRIOR)
	$monstertypenames .= ' Warrior';

if ($monstertype & MONSTERTYPE_SPELLCASTER)
	$monstertypenames .= ' Spellcaster';

if ($monstertype & MONSTERTYPE_FAIRY)
	$monstertypenames .= ' Fairy';

if ($monstertype & MONSTERTYPE_FIEND)
	$monstertypenames .= ' Fiend';

if ($monstertype & MONSTERTYPE_ZOMBIE)
	$monstertypenames .= ' Zombie';

if ($monstertype & MONSTERTYPE_MACHINE)
	$monstertypenames .= ' Machine';

if ($monstertype & MONSTERTYPE_AQUA)
	$monstertypenames .= ' Aqua';

if ($monstertype & MONSTERTYPE_PYRO)
	$monstertypenames .= ' Pyro';

if ($monstertype & MONSTERTYPE_ROCK)
	$monstertypenames .= ' Rock';

if ($monstertype & MONSTERTYPE_WINGEDBEAST)
	$monstertypenames .= ' WingedBeast';

if ($monstertype & MONSTERTYPE_PLANT)
	$monstertypenames .= ' Plant';

if ($monstertype & MONSTERTYPE_INSECT)
	$monstertypenames .= ' Insect';

if ($monstertype & MONSTERTYPE_THUNDER)
	$monstertypenames .= ' Thunder';

if ($monstertype & MONSTERTYPE_DRAGON)
	$monstertypenames .= ' Dragon';

if ($monstertype & MONSTERTYPE_BEAST)
	$monstertypenames .= ' Beast';

if ($monstertype & MONSTERTYPE_BEASTWARRIOR)
	$monstertypenames .= ' BeastWarrior';

if ($monstertype & MONSTERTYPE_DINOSAUR)
	$monstertypenames .= ' Dinosaur';

if ($monstertype & MONSTERTYPE_FISH)
	$monstertypenames .= ' Fish';

if ($monstertype & MONSTERTYPE_SEASERPENT)
	$monstertypenames .= ' Sea Serpent';

if ($monstertype & MONSTERTYPE_REPTILE)
	$monstertypenames .= ' Reptile';

if ($monstertype & MONSTERTYPE_PSYCHIC)
	$monstertypenames .= ' Psychic';

if ($monstertype & MONSTERTYPE_DIVINE)
	$monstertypenames .= ' Divine';

if ($monstertype & MONSTERTYPE_CREATORGOD)
	$monstertypenames .= ' CreatorGod';

if ($monstertype & MONSTERTYPE_WYRM)
	$monstertypenames .= ' Wyrm';

if ($monstertype & MONSTERTYPE_CYBERSE)
	$monstertypenames .= ' Cyberse';

/* Get card types */
$cardtype = $carddata['cardtype'];
$cardtypeicons = '';
$maincardtype = false;
$spelltraptype = '';
if ($cardtype & CARDTYPE_MONSTER) {
	$maincardtype = CARDTYPE_MONSTER;
	$cardtypeicons .= '[Monster]';
	$monsterleveltypeattribute = "Level {$carddata['level']} ".mb_strtoupper($attributename);
}

if ($cardtype & CARDTYPE_SPELL) {
	$maincardtype = CARDTYPE_SPELL;
	$cardtypeicons .= '[Spell]';
	$spelltraptype = 'Spell';
}

if ($cardtype & CARDTYPE_TRAP) {
	$maincardtype = CARDTYPE_TRAP;
	$cardtypeicons .= '[Trap]';
	$spelltraptype = 'Trap';
}

if ($cardtype & CARDTYPE_NORMAL) {
	$cardtypeicons .= '[Normal]';
	$monstertypenames .= ' / Normal';
}

if ($cardtype & CARDTYPE_FUSION) {
	$cardtypeicons .= '[Fusion]';
	$monstertypenames .= ' / Fusion';
}

if ($cardtype & CARDTYPE_RITUAL) {
	$cardtypeicons .= '[Ritual]';
	$monstertypenames .= ' / Ritual';
}

if ($cardtype & CARDTYPE_TRAPMONSTER)
	$cardtypeicons .= '[TrapMonster]';

if ($cardtype & CARDTYPE_SPIRIT) {
	$cardtypeicons .= '[Spirit]';
	$monstertypenames .= ' / Spirit';
}

if ($cardtype & CARDTYPE_UNION) {
	$cardtypeicons .= ' / Union';
	$monstertypenames .= ' / Union';
}

if ($cardtype & CARDTYPE_GEMINI) {
	$cardtypeicons .= '[Gemini]';
	$monstertypenames .= ' / Gemini';
}

if ($cardtype & CARDTYPE_SYNCHRO) {
	$cardtypeicons .= '[Synchro]';
	$monstertypenames .= ' / Synchro';
}

if ($cardtype & CARDTYPE_TUNER) {
	$cardtypeicons .= '[Tuner]';
	$monstertypenames .= ' / Tuner';
}

if ($cardtype & CARDTYPE_TOKEN)
	$cardtypeicons .= '[Token]';

if ($cardtype & CARDTYPE_QUICKPLAY) {
	$cardtypeicons .= '[QuickPlay]';
	$spelltraptype = 'Quick Play '.$spelltraptype;
}

if ($cardtype & CARDTYPE_CONTINUOUS) {
	$cardtypeicons .= '[Continuous]';
	$spelltraptype = 'Continuous '.$spelltraptype;
}

if ($cardtype & CARDTYPE_EQUIP) {
	$cardtypeicons .= '[Equip]';
	$spelltraptype = 'Equip '.$spelltraptype;
}

if ($cardtype & CARDTYPE_FIELD) {
	$cardtypeicons .= '[Field]';
	$spelltraptype = 'Field '.$spelltraptype;
}

if ($cardtype & CARDTYPE_COUNTER) {
	$cardtypeicons .= '[Counter]';
	$spelltraptype = 'Counter '.$spelltraptype;
}

if ($cardtype & CARDTYPE_FLIP) {
	$cardtypeicons .= '[Flip]';
	$monstertypenames .= ' / Flip';
}

if ($cardtype & CARDTYPE_TOON) {
	$cardtypeicons .= '[Toon]';
	$monstertypenames .= ' / Toon';
}

if ($cardtype & CARDTYPE_XYZ) {
	$cardtypeicons .= '[Xyz]';
	$monstertypenames .= ' / Xyz';
}

if ($cardtype & CARDTYPE_PENDULUM) {
	$cardtypeicons .= '[Pendulum]';
	$monstertypenames .= ' / Pendulum';
}

if ($cardtype & CARDTYPE_SPSUMMON)
	$cardtypeicons .= '[SpecialSummon]';

$islinkmonster = false;
if ($cardtype & CARDTYPE_LINK) {
	$cardtypeicons .= '[Link]';
	$monstertypenames .= ' / Link';
	$islinkmonster = true;
}

if ($cardtype & CARDTYPE_EFFECT) {
	$cardtypeicons .= '[Effect]';
	$monstertypenames .= ' / Effect';
}

/* Get link arrows from defence. */
$def = $carddata['def'];
$linkarrows = '';
$linkrating = 0;
if ($def & LINK_MARKER_LEFT) {
	$linkarrows .= ' `←`';
	$linkrating++;
}

if ($def & LINK_MARKER_BOTTOM_LEFT) {
	$linkarrows .= ' `↙`';
	$linkrating++;
}

if ($def & LINK_MARKER_BOTTOM) {
	$linkarrows .= ' `↓`';
	$linkrating++;
}

if ($def & LINK_MARKER_BOTTOM_RIGHT) {
	$linkarrows .= ' `↘`';
	$linkrating++;
}

if ($def & LINK_MARKER_RIGHT) {
	$linkarrows .= ' `→`';
	$linkrating++;
}

if ($def & LINK_MARKER_TOP_RIGHT) {
	$linkarrows .= ' `↗`';
	$linkrating++;
}

if ($def & LINK_MARKER_TOP) {
	$linkarrows .= ' `↑`';
	$linkrating++;
}

if ($def & LINK_MARKER_TOP_LEFT) {
	$linkarrows .= ' `↖`';
	$linkrating++;
}

$rarityedition = '';
$titledata = "{$carddata['setprefix']}-{$carddata['setnumber']}";
if ($carddata['raritycode'] && $carddata['editioncode']) {
	$rarityedition = " / {$carddata['rarity']} / {$carddata['edition']}";
	$titledata .= " {$carddata['rarity']} {$carddata['edition']}";
} else if ($carddata['raritycode']) {
	$rarityedition = ' / '.$carddata['rarity'];
	$titledata .= ' '.$carddata['rarity'];
} else if ($carddata['editioncode']) {
	$rarityedition = ' / '.$carddata['edition'];
	$titledata .= ' '.$carddata['edition'];
}

$basepath = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME']
		.pathinfo($_SERVER['REQUEST_URI'], PATHINFO_DIRNAME);
$cardtext = str_replace(["\r\n", "\n\r", "\n", "\r"], '<br>', $carddata['cardtext']);

$atk = $carddata['atk'];
if ($atk == -2) $atk = '?';

$def = $carddata['def'];
if ($def == -2) $def = '?';

?># <?=$carddata['cardname']?> <?=$titledata?><br><br><img width="177" height="254" src="<?=$basepath?>/cardpics/<?=$carddata['setprefix']?>/<?=$carddata['cardid']?>.jpg" style="float:left;padding:0 1em 0.5em 0;">**<span itemprop="price" content="<?=$price?>"><meta itemprop="priceCurrency" content="AUD"><?=$priceformatted?></span>** &bull; <span itemprop="size">1</span> available<br>High:&nbsp;<?=$pricehighformatted?> / Low:&nbsp;<?=$pricelowformatted?> / Avg:&nbsp;<?=$priceaverageformatted?><br>Source: <a href='https://yugiohprices.com/'>yugiohprices.com</a><br><br>Set: <?=$carddata['setname']?><br><?=$carddata['setprefix']?>-<?=$carddata['setnumber']?><?=$rarityedition?><br><?php
if ($maincardtype == CARDTYPE_MONSTER) {
?> <?=$monsterleveltypeattribute?> <?=$monstertypenames?><br>ATK/<?=$atk?> &nbsp; <?php
	if (!$islinkmonster) {
?>DEF/<?=$def?><br><?php
	} else {
?>LINK-<?=$linkrating?><br>Link Arrows: <?=$linkarrows?><br><?php
	}
} else {
?><?=$spelltraptype?><br><?php
}
?><br><?=$cardtext?>
